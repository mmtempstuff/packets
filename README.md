### Description

The project builds a small CLI executable *packets* (*packets.exe* on Win). The user is first asked to start processing by entering `'s'`. *packets* then starts two background threads: one that produces and adds packets to processing and the other that processes the incoming packets. Packets are produced at higher rate (50ms) than they are processed. A certain percentage of the produced packets are failed. Processing of regular packets is simulating by 100ms wait period. User can always break the processing by entering `'a'` at the command line, triggering a special 'abort' packet, that gracefully stops the processing. A cumulative number of the failed packets is shown at the command line.

### Source code

*main.cpp* starts the production and processing of packets and waits for the user's triggered abortion.

*process.hpp* and *process.cpp* implement a `Process` class that represents a simple processing of incoming packets.

*packet.hpp* and *packet.cpp* define an abstract base class `IPacket` as well as three derived classes `RegularPacket`, `IregularPacket` and `AbortPacket` that simulate a normal functionality, throw exception and abort processing respectively.

### Build

```
mkdir build
 cd build
 cmake ..
 make
 ```

 Code is also running in Windows, simply by cloning or downloading the project, opening it in VisualStudio 2019 as a folder and compiling it.

 ### Run

```
./packets
Enter 's' to start processing, than enter 'a' to abort processing.
s
Packet added.
Processing a Regular packet.
Packet added.
Processing a Regular packet.
Packet added.
Packet added.
An iregular packet throws an exception.
.
.
.

a
.
.
.

isIdle  : 1
An abort packet gracefully stopped the processing.
isIdle  : 0
failedPackets  : 33

```


