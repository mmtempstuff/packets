#include <chrono>

#include "process.hpp"
#include "packet.hpp"

Process::Process()
{
    failedPackets_ = 0;
    idle_ = true;

    // start process thread
    t_ = std::make_unique<std::thread>(&Process::Processing_, this);
}

void Process::AddPacket(std::unique_ptr<IPacket> packet)
{
    // push the packet into queue
    std::lock_guard<std::mutex> lock(queueLock_);
    packetsQueue_.push(std::move(packet));
}

bool Process::IsIdle()
{
    return !(packetsQueue_.size() == 0 && !idle_);
}

size_t Process::FailedPacketsNumber()
{
    return failedPackets_;
}

void Process::Abort()
{
    // empty the queue
    std::queue<std::unique_ptr<IPacket>> empty;
    packetsQueue_.swap(empty);

    // push an 'abort' Packet into emptied queue
    std::unique_lock<std::mutex> lock(queueLock_);
    packetsQueue_.push(std::make_unique<AbortPacket>());
    lock.unlock();

    // join processing thread
    t_->join();
}

void Process::Processing_()
{
    // process incoming tasks until aborted
    while(true)
    {
        //check for and pop next task from the queue 
        //(using c++ conditional variables instead of yield would be CPU friendlier)
        while(packetsQueue_.empty())
            std::this_thread::yield();

        idle_ = false;
        std::unique_lock<std::mutex> lock(queueLock_);
        auto packet = std::move(packetsQueue_.front());
        packetsQueue_.pop();
        lock.unlock();

        //process the task
        try
        {
            packet->Process();
        }
        catch(const std::runtime_error& e)
        {
            std::string_view exc{e.what()};
            if(exc == "EXCEPTION")
                failedPackets_++;
            else if(exc == "ABORT")
                break;
            else
                throw;            
        }            
        idle_ = true;
    } 
}