
class IPacket
{
public:
    virtual int Process() = 0;
};

class RegularPacket : public IPacket
{
public:
    int Process();
};

class IregularPacket : public IPacket
{
public:
    int Process();
};

class AbortPacket : public IPacket
{
public:
    int Process();
};

