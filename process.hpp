#include <memory>
#include <queue>
#include <thread>
#include <mutex>

class IPacket;

class Process
{
public:
    Process();
    void AddPacket(std::unique_ptr<IPacket> packet);
    bool IsIdle();
    size_t FailedPacketsNumber();
    void Abort();

private:
    std::queue<std::unique_ptr<IPacket>> packetsQueue_;
    std::mutex queueLock_;
    size_t failedPackets_;
    std::unique_ptr<std::thread> t_;
    bool idle_;

    void Processing_();
};