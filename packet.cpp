#include <iostream>
#include <stdexcept>

#include <chrono>
#include <thread>

#include "packet.hpp"

int RegularPacket::Process()
{
    std::cout << "Processing a Regular packet.\n"; 
    std::this_thread::sleep_for (std::chrono::milliseconds(100));
    return 0;
}

int IregularPacket::Process()
{
    std::cout << "An iregular packet throws an exception.\n";
    throw std::runtime_error("EXCEPTION");
    return 0;
}

int AbortPacket::Process()
{
    std::cout << "An abort packet gracefully stopped the processing.\n";
    throw std::runtime_error("ABORT");
    return 0;
}
