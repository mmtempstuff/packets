#include <iostream>
#include <csignal>

#include "process.hpp"
#include "packet.hpp"

int main(int argc, char** argv) 
{
    unsigned char s, a;
    std::cout << "Enter 's' to start processing, than enter 'a' to abort processing.\n";
    do { std::cin >> s; } while (s != 's' && s != 'S');

    Process packets;

    // start adding packets in a separate thread
    std::thread t([&]()
    {
        srand(time(0));
        while(a != 'a' && a != 'A')
        {
            std::cout << "Packet added.\n";
            if(rand()%4 == 0)   // cca. 1/4 of packets are 'failed'
            {
                packets.AddPacket(std::make_unique<IregularPacket>());
            }
            else
            {
                packets.AddPacket(std::make_unique<RegularPacket>());
            }
            std::this_thread::sleep_for (std::chrono::milliseconds(50));
        }
    });

    // wait user to abort processing
    do { std::cin >> a; } while (a != 'a' && a != 'A');

    std::cout << "isIdle  : " << packets.IsIdle() << std::endl;

    packets.Abort();
    t.join();

    std::cout << "isIdle  : " << packets.IsIdle() << std::endl;
    std::cout << "failedPackets  : " << packets.FailedPacketsNumber() << std::endl;

    return 0;
}
